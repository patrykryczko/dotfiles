# variables
export PATH=$HOME/bin:/usr/local/bin:$HOME/.cargo/bin:$HOME/.node_modules/bin:$HOME/.go/bin:/home/patryk/.yarn/bin:$HOME/.local/bin:$PATH
export ZSH="$HOME/.oh-my-zsh"

# Go
export GOPATH=$HOME/.go

# Deno
export DENO_INSTALL="/home/patryk/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

# Node Version Manager
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" 

# Android SDK
export ANDROID_SDK_ROOT=$HOME/Apps/Android/Sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin
export CHROME_EXECUTABLE=/opt/google/chrome/google-chrome

# plugins
plugins=(
    z

    # fish-like autosuggestions -> https://github.com/zsh-users/zsh-autosuggestions
    # install it with 'git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions'
    zsh-autosuggestions

    # fish-like syntax highlighting
    # install it with 'git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting'
    zsh-syntax-highlighting
)

# load theme -> https://starship.rs/
# install it with 'curl -fsSL https://starship.rs/install.sh | bash'
eval "$(starship init zsh)"

# load aliases
source $HOME/.aliases

source $ZSH/oh-my-zsh.sh
