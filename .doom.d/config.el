;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; misc

;; Doom
(setq
 doom-theme 'gruvbox-light-medium
 doom-font (font-spec :family "JetBrains Mono" :size 15)
 doom-big-font (font-spec :family "JetBrains Mono" :size 15))

;; UI
(setq
 whitespace-line-column 80
 display-line-numbers-type 'relative
 whitespace-styl '(face empty tabs lines-tail trailing))

;; Whitespace
(require 'whitespace)
(global-whitespace-mode t)

;; Language-specific
(setq
 lsp-rust-server 'rust-analyzer)

;; Org-roam
(setq
 org-roam-directory "~/Documents/Notes/")

(setq org-roam-dailies-capture-templates
      '(("d" "default" entry
         #'org-roam-capture--get-point
         "* %?"
         :file-name "daily/%<%Y-%m-%d>"
         :head "#+title: %<%Y-%m-%d>\n\n")))

(require 'org-roam-protocol)

;; Use the $PATH set up by the user's shell
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
