;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

(package! gruvbox-theme)
(package! exec-path-from-shell)
