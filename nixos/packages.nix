{ config, pkgs, ... }:

{
  # List packages installed in system profile
  environment.systemPackages = with pkgs; [
     # System utils
     mc
     wget
     htop
     bat
     nnn # File manager
     unrar
     unzip
     zip
     ripgrep
     fd
     aspell
     aspellDicts.en
     aspellDicts.en-computers

     # Editors
     nano
     emacs

     # Shell
     zsh-z
     zsh-autosuggestions
     zsh-syntax-highlighting
     thefuck
     starship

     # Multimedia
     llpp # PDF reader
     youtube-dl
     subdl
     mpv
     firefox
     qbittorrent
     syncthing

     #Developmetns
     sqlite
     vscode
   ];

   fonts = {
    fonts = with pkgs; [
     jetbrains-mono
     nerdfonts
    ];
  };
}
