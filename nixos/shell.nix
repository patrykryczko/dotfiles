{ config, pkgs, ... }:

{
  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    autosuggestions.enable = true;
    ohMyZsh = {
      enable = true;
      theme = "lambda";
      plugins = [ "thefuck" "z" ];
    };
    shellAliases = {
      # Git
      gs = "git status";
      grlc = "git reset --soft HEAD\^";

      # Nix
      nec = "sudo vim /etc/nixos/configuration.nix";
      nesh = "sudo vim /etc/nixos/shell.nix";
      nes = "sudo vim /etc/nixos/services.nix";
      ned = "sudo vim /etc/nixos/desktop.nix";
      nep = "sudo vim /etc/nixos/packages.nix";
      neu = "sudo vim /etc/nixos/users.nix";
      nrs = "sudo nixos-rebuild switch";
      ns = "nix-shell";

      # FLutter
      fr = "flutter run";
      ft = "flutter test";
      ftc = "flutter test --coverage && genhtml coverage/lcov.info -o coverage/html";
      fbr = "flutter pub run build_runner build";
    };
    promptInit = ''
      eval "$(starship init zsh)"
    '';
    shellInit = ''
      export BAT_THEME="gruvbox-light"
    '';
  };
}
