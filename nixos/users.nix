{ config, lib, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.patryk = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" "video" ];
  };

  home-manager.users.patryk = { pkgs, ... }: {
    programs.vim = {
      enable = true;
      settings = {
        number = true;
      };
      extraConfig = ''
        set number
      '';
    };

    programs.git = {
      enable = true;
      userName = "Patryk Ryczko";
      userEmail = "patryk.ryczko@protonmail.com";
    };
  };
}
